#include <iostream>
#include "person.hpp"

int main(int, char**) 
{
 	Person anonimous;
 	anonimous.Greetings();
 	string name = "Bob";

 	Student modelStudent (21, name, 5);
 	modelStudent.Greetings();
 	cout << "Model student " << modelStudent.Name() << " has average mark " << modelStudent.Mark() << endl;
 	Teacher someone(64, name+"-senior");
 	cout << "First, as a teacher: " << endl;
 	someone.Greetings();
 	cout << "Then, as a person: " << endl;
 	someone.Person::Greetings();
    Assistant assist(23, "Alice", 4);
    assist.Greetings();

    cout << "Size of Person " << sizeof(Person) << endl;
    cout << "Size of Student " << sizeof(Student) << endl;
    cout << "Size of Teacher " << sizeof(Teacher) << endl;
    cout << "Size of Assistant " << sizeof(Assistant) << endl;
 	return 0;
}
