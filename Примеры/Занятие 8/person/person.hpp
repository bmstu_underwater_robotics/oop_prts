#ifndef PERSON_H
#define PERSON_H

#include <string>
#include <iostream>
using namespace std;

class Person
{
public:
 	Person();
 	Person(int age, string name);
    ~Person();
 	int Age() const {return m_Age;}
 	string Name() const {return m_Name;}
 	void Greetings() const;
protected:
 	int m_Age;
 	string m_Name;
};

class Student : public Person
{
public:
 	Student(int age, string name, int mark);
    ~Student();
 	int Mark() const {return m_AverageMark;}
protected:
 	int m_AverageMark;
};

class Teacher : public Person
{
public:
 	Teacher(int age, string name) :  Person(age, name) {cout << "Teacher c-tor called for " << m_Name <<endl;}
 	~Teacher() {cout << "Teacher d-tor called for "<< m_Name <<endl;}
 	void Greetings();
};

class Assistant : public Student, public Teacher
{
public:
 	Assistant(int age, string name, int mark);
    ~Assistant();
    void Greetings();
};

#endif