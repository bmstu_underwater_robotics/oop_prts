#include "person.hpp"
#include <iostream>

Person::Person()
{
 	m_Name = "Anonymous";
 	m_Age = -1;
    cout << "Person c-tor called for " << m_Name << endl;
}

Person::Person(int age, string name)
{
 	m_Age = age;
 	m_Name = name;
    cout << "Person c-tor called for " << m_Name << endl;
}

Person::~Person() 
{
    cout << "Person d-tor called for " << m_Name << endl;
}

void Person::Greetings() const
{
 	cout << "Hello, guys, my name is " << m_Name << endl;
}

Student::Student(int age, string name, int mark)
 	: Person(age, name)
{
 	m_AverageMark = mark;
    cout << "Student c-tor called for " << m_Name << endl;
}

Student::~Student()
{
    cout << "Student d-tor called for " << m_Name << endl;
}

void Teacher::Greetings()
{
 	cout << "Hello, students, let's start our lesson..." << endl;
}

Assistant::Assistant(int age, string name, int mark)
: Student(age, name, mark), Teacher(age, name)
{
    cout << "Assistant c-tor called for " << name << endl;
}

Assistant::~Assistant()
{
    cout << "Assistant d-tor called for " << Student::m_Name << endl;
}

void Assistant::Greetings()
{
    Teacher::Greetings();
}
