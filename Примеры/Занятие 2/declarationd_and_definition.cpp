#include <iostream>
using namespace std;
//  Объявление функции - только сигнатура
int printFibonacci(int num);
const int maxNumber=10;
int main()
{
    int lastNumber = printFibonacci(maxNumber);
    //  Мы можем использовать функцию, которую объявили
    cout << "Last calculated number " << lastNumber << endl;
    return 0;
}
//Определение (реализация) функции
int printFibonacci(int num)
{
    int first=1, second=1;
    cout << "1 "<< first << endl;
    cout << "2 "<<second << endl;
    int next = 0;
    for (int i = 2; i< num; i++)
    {
        next = first + second;
        cout << i+1 << " " << next << endl;
        first = second; second = next;
    }
    return next;
}
