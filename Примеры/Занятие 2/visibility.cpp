#include <iostream>
using namespace std;
int someVariable;
int main(int argc, char *argv[])
{
 	int anotherVariable = 1;
 	cout << "Some variable " << someVariable << endl;
 	cout << "Other variable " << anotherVariable << endl;
 	float someVariable = 3.14;
 	cout << "Some variable " << someVariable << endl;
 	for (int i=0; i<2; i++)
 	{
 		int j = i;
 		cout << "i=" <<i << " j=" << j << endl;
 		float anotherVariable = 2.78;
 		cout << "Other variable " << anotherVariable << endl;
 	}
 	cout << "Other variable " << anotherVariable << endl;
	return 0;
}
