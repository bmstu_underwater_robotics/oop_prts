#include <iostream>

using namespace std;

int factorial (int num)
{
    if (num == 1)
        return 1;
    else
        return factorial(num-1)*num;
}

int main()
{
    int lastNumber = factorial(6);
    cout << "Last calculated number " << lastNumber << endl;
    return 0;
}
