#include <iostream>
using namespace std;

unsigned int fibonacci(unsigned int n)
{
  if (n ==1 || n==2)
    return 1;
  if (n > 2)
    return fibonacci(n-1) + fibonacci(n-2);
}

const int maxNumber=10;

int main()
{
  int lastNumber = fibonacci(maxNumber);
  // Мы можем использовать функцию, которую объявили
  cout << "Last calculated number " << lastNumber << endl;
  return 0;
}
