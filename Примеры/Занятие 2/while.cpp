#include <iostream>
using namespace std;

bool isEven(int num)
{
    if (num % 2 ==1)
    {
        cout << num << " is odd" << endl;
        return false;
    }
    else
    {
        cout << num << " is even" << endl;
        return true;
    }
}

int main()
{
    int maxNumber = 10;
    int number = 0;
    while (number < maxNumber)
    {// Выполняем, пока number меньше maxNumber
        isEven(number);
        number++;
    }
    return 0;
}
