#include <iostream>
#define _USE_MATH_DEFINES
#include <cmath>
using namespace std;

double function (double arg);

int main()
{
    double eps = 0.01;
    double leftBound = 0,
           rightBound = 2;
    if (function(leftBound)*function(rightBound)<0)
    {
        //  Ваш код    
    }
    else
        cout << "Unable to find solution on interval ["<< leftBound<<" , "<< rightBound<<"]\n";
    return 0;
}
double function (double x)
{
    return (-exp(x)+2*cos(x));
}
