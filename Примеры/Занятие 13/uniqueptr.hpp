#pragma once

template <typename T>
class UniquePtr
{
public:
    explicit UniquePtr(T* ptr = nullptr) : m_ptr(ptr) {}
    ~UniquePtr()
    {
        if (m_ptr)
            delete m_ptr;
    }
    T* get() const {return m_ptr;}
    T* release() 
    {
        T* newPtr = m_ptr;
        m_ptr = nullptr;
        return newPtr;
    }
    void reset(T* ptr = nullptr)
    {
        if (m_ptr)
            delete m_ptr;
        m_ptr = ptr;
    }

    UniquePtr(const UniquePtr<T>&) = delete;
    UniquePtr& operator=(const UniquePtr<T>&) = delete;

    template<typename U>
    UniquePtr(UniquePtr<U>&& other)
    {
        this->m_ptr = static_cast<T*>(other.release());
    }
    template <typename U>
    UniquePtr& operator=(UniquePtr<U>&& rhs)
    {
        if (static_cast<void*>(this) != static_cast<void*>(&rhs))
        {
            reset(static_cast<T*>(rhs.release()));
        }
        return *this;
    }

    UniquePtr& operator=(nullptr_t)
    {
        reset(nullptr);
        return *this;
    }

    T& operator*() const 
    {
        return *m_ptr;
    }
    T* operator->() const 
    {
        return m_ptr;
    }
    explicit operator bool() 
    {
        return (get()!=nullptr);
    }

protected:
    T* m_ptr;
};
