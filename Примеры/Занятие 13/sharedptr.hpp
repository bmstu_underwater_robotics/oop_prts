#pragma once

struct RefCounter
{
    RefCounter(int cnt = 0) : m_cnt(cnt) {}
    ~RefCounter() = default;
    int m_cnt = 0;
};


template <typename T>
class SharedPtr
{
private:
    // template <typename U>    
    void decrease()
    {
        if (m_ptr)
        {
            m_ref->m_cnt--;
            if (m_ref->m_cnt==0)
            {
                delete m_ptr;
                m_ptr = nullptr;
            }
        }
    }
    template <typename U>
    void increase(SharedPtr<U> const & ptr)
    {
        m_ptr = static_cast<T*>(ptr.m_ptr);
        if (m_ptr)
        {
            if (m_ref && m_ref->m_cnt==0)
                delete m_ref;
            m_ref = ptr.m_ref;
            m_ref->m_cnt++;
        }
    }
    
    T* m_ptr;
    mutable RefCounter* m_ref;
public:
    explicit SharedPtr(T *ptr = 0) : m_ptr(ptr)
    {
        m_ref = new RefCounter(m_ptr? 1 : 0);
    }
    ~SharedPtr()
    {
        decrease();
        if (m_ref->m_cnt==0)
        {
            delete m_ref;
        }
    }
    template <class U>
    SharedPtr(const SharedPtr<U> & other)
    {
        increase(other);
    }
    template <class U>
    SharedPtr& operator=(const SharedPtr<U> & rhs)
    {
        if (static_cast<void*>(this)!= static_cast<void const *>(&rhs))
        {
            decrease();
            increase(rhs);
        }
        return *this;
    }

    T* get() const
    {
        if (m_ptr)
            return m_ptr;
        else return static_cast<T*>(nullptr);
    }
    void reset(T *ptr = 0)
    {
        decrease();
        m_ptr = ptr;
        if (m_ptr)
        {
            if (m_ref && m_ref->m_cnt==0)
                delete m_ref;
            m_ref = new RefCounter(1);
        }
    }
    T& operator*() const
    {
        return *(m_ptr);
    }
    T* operator->() const
    {
        return m_ptr;        
    }

    template <typename U>
    friend class SharedPtr;  
};