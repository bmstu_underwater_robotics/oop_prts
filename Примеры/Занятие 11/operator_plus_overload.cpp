#include <iostream>
#include <string>

using namespace std;

class MyString
{
public:
    MyString (const char* str = "")
    {
        m_length = strlen (str)+1;
        m_pPtr = new char [m_length];
        strcpy(m_pPtr, str);
    }
    ~MyString () {if (m_pPtr) delete[] m_pPtr;}
    void append (const char* str)
    {
        int newLength = m_length+strlen(str);
        char* newString = new char[newLength];
        strcpy(newString, m_pPtr);
        strcat(newString, str);
        delete[] m_pPtr;
        m_pPtr = newString;
        m_length = newLength;
    }
    char* getStringPtr() {return m_pPtr;}
    MyString (MyString const& str)
    {
        m_length = str.m_length;
        m_pPtr = new char [m_length];
        strcpy(m_pPtr, str.m_pPtr);
    }
    MyString& operator= (MyString const& rhs)
    {
        if (this != &rhs)
        {
            if (m_pPtr)
                delete m_pPtr;
            m_length = rhs.m_length;
            m_pPtr = new char [m_length];
            strcpy(m_pPtr, rhs.m_pPtr);                   
        }
        return *this;
    }
    friend MyString operator+ (MyString const& lhs, MyString const& rhs);
private:
    char* m_pPtr;
    int m_length;
};

MyString operator+ (MyString const& lhs, MyString const& rhs)
{
    MyString ret (lhs);
    ret.append(rhs.m_pPtr);
    return ret;
}

int main()
{
	MyString a = "Some text";
	MyString b = a + " was concatenated";
	cout << b.getStringPtr() << endl;
	MyString c = "Another " + b;	//	Ошибка нет
	cout << c.getStringPtr() << endl;
	return 0;
}