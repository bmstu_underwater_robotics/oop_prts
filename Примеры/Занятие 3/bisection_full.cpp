#include <cmath>
#include <iostream>
using namespace std;

double function(double x);

int main()
{
    const double eps = 0.01;
    float leftBound = 0, rightBound = 2;
    int i = 0;
    if (function(leftBound)*function(rightBound)<0)
    {
        float midX;
        do
        {
            midX = (leftBound+rightBound) / 2;
            if (function(midX)*function(rightBound)<0)
                 leftBound = midX;
            else
                rightBound = midX;
            cout << ++i <<": x=" << midX << " f(x)=" << function(midX) << endl;
        }
        while(fabs(function(midX))>eps);
    }
    else
    {
        cout << "Unable to find root on interval ["
             << leftBound << ", "<< rightBound << "]" << endl;
    }
    return 0;
}

double function (double x)
{
    return (-exp(x) + 2 * cos(x));
}
