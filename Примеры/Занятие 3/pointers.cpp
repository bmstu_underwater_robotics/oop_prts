#include <iostream>
using namespace std;

int main()
{
    int a = 10;     //  Переменная типа int
    int* intPtr;    //  Указатель на переменную типа int
    cout << "Value of a = "<< a << endl;
    cout << "Value of intPtr =" << intPtr << endl;  //  Адрес, на который указываем
    //    cout << "Value addresed in intPtr =" << *intPtr << endl;    //  Значение по адресу
    //  Вот тут программа могла упасть
    cout << "Address of variable a = " << &a << endl;   //  Адрес переменной a
    intPtr = &a;        //  Присвоение адреса указателю
    cout << "New value of intPtr =" << intPtr << endl;
    cout << "New value addresed in intPtr =" << *intPtr << endl;
    *intPtr = 24;       //  Изменеие ЗНАЧЕНИЯ по адресу
    cout << "New value addresed in intPtr =" << *intPtr << endl;
    cout << "V of a =" << a << endl;
}
