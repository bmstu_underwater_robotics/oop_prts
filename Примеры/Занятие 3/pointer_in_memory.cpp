#include <iostream>
using namespace std;
int main()
{
    int a = 11;
    unsigned char* charPtr = nullptr;   //  Инициализация нулевым указателем
//    charPtr = (unsigned char*) &a; // Преобразование типа в стиле C
    // Преобразование типа в стиле C++
    charPtr = reinterpret_cast<unsigned char*>(&a);
    cout << "Size of int type "<< sizeof(int)        << " bytes"<< endl;
    cout << "Size of char type " << sizeof(*charPtr) << " bytes"<< endl;
    cout << "Size of pointer " << sizeof(charPtr)    << " bytes"<< endl;
    cout << "a = " << dec << a << endl;
    for (int i=0; i < sizeof(int); i++)
    {
        cout << dec << i << "-th byte value is " 
	<< hex << (int)*(charPtr+i) << endl;
    }
    a = a +512;
    cout << "Now a = " << dec << a << endl;
    for (int i=0; i < sizeof(int); i++)
    {
        cout << dec << i << "-th byte value is " 
	<< hex << (int)*(charPtr+i) << endl;
    }
}
