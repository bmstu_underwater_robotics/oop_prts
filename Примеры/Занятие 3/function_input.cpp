#include <cmath>
#include <iostream>
using namespace std;

void testFunction (int a)
{
    a = 10;
    cout << "Var in function " << a << endl;
}

int main()
{
    int var = 0;
    cout << "Var before function " << var << endl;
    testFunction(var);
    cout << "Var after function " << var << endl;
}
