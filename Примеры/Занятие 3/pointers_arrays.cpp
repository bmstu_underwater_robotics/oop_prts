#include <iostream>
using namespace std;

int main(int, char**) 
{
    const int size = 10;
    int array[size];
    //  Мы инициализируем элементы массива, обращаясь к ним через оператор индексирования []
    for (int i=0; i<size; i++)
        array[i] = size - i;   
    cout << "Array size "<< sizeof(array) << " and element size " << sizeof(array[0]) << endl;
    //  С массивом можно работать как с обычным указателем - вместо индексирования
    for (int i=0; i<size; i++)  //  Хотя так менее удобно
        cout << i <<"-th element " << *(array+i) << endl;
    
    int* ptr = array;   //  А это указатель, указывающий на первый элемент
    for (int i=0; i<size; i++)  //  Хотя это и указатель, мы можем его индексировать
        ptr[i] *= 2;    //  Умножили все элементы на 2
    cout << "Value of first element " << array[0] << endl;   //  Первый элемент
    cout << "Value of last element " << array[size-1] << endl;   //  Последний элемент
    cout << "Value AFTER last element " << array[size] << endl;  //  Частая ошибка
}
