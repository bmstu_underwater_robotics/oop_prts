#include <iostream>
using namespace std;

int gSize = 10;   
void fibonacci(int array[], int* size);

int main(int, char**) 
{
    int array [gSize];
    cout << "Array is pointing to " << array << " and its address " << &array << endl;
    cout << "Size address is " << &gSize << " and value " << gSize << endl;
    for (int i =0; i< gSize; i++)
        array[i] = 0;
    fibonacci(array, &gSize);
    for (int i =0; i< gSize; i++)
        cout << i <<"-th fibonacci number "<< array[i] << endl;
    cout << "Size value " << gSize << endl;
}

void fibonacci(int array[], int* size)
{
    cout << "Array is pointing to " << array  << " and its address " << &array << endl;
    cout << "Size address is " << &size << " and value " << size << endl;
    if (*size <=2)
        return;
    array [0] = array[1] = 1;
    for (int i = 2; i< *size; i++)
        array[i] = array[i-1] + array[i-2];
    *size = *size + 2;
}
