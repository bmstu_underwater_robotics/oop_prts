#include <chrono>
using namespace std;
using namespace std::chrono;
const int maxNumber=10;

int main()
{
    auto t1 = system_clock::now();
    //  Какой-то код
    //  ...
    auto lastNumber = fibonacci(45);
    auto t2 = system_clock::now();
    cout <<"Last calculated number " << lastNumber << endl;
    cout << "Time taken " << (t2-t1).count() << " ns"<< endl;
    return 0;
}
