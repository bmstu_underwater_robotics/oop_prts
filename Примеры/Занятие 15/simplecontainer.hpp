#pragma once
#include <stdexcept>
#include <string>
#include <iterator>
using namespace std;

template <typename T>
class SimpleContainer
{
public:
    using value_type = T;

    class const_iterator
    {
        T* m_ptr;
    public:
        using iterator_category = bidirectional_iterator_tag;
        using value_type = T;
        using difference_type = void;
        using pointer = T*;
        using reference = T&;

        const_iterator(T* ptr = nullptr) : m_ptr(ptr){}
        //  ...
    };

 	SimpleContainer() : m_size(0), m_capacity(10)
    {
        m_data = new T[m_capacity];
    }
 	SimpleContainer(int size, T value=T())
        : m_size(size), m_capacity(size)
    {
        m_data = new T[m_capacity];
        for (T& v : data)
            v = value;
    }
 	SimpleContainer(const SimpleContainer& other)
        :   m_capacity(other.m_capacity), m_size(other.m_size)
    {
        m_data = new T[m_capacity];
  	    std::copy(other.m_data, other.m_data+m_size, m_data);       
    }
 	~SimpleContainer()
    {
        if (m_data)
            delete m_data;
    }
 	SimpleContainer& operator=(const SimpleContainer& rhs)
    {
        if (this != &rhs)
        {
            if (m_data)
                delete[] m_data;
            m_capacity = rhs.m_capacity;
            m_size = rhs.m_size;
            m_data = new T[m_capacity];
  	        std::copy(rhs.m_data, rhs.m_data+m_size, m_data); 
        }
        return *this;
    }

 	int size() const {return m_size;}
 	int capacity() const {return m_capacity;}
 	T get(int index) const
    {
        if (index<0 || index>=m_size)
            throw std::out_of_range("SampleContainer: Wrong index " + to_string(idx));
        return m_data[index];
    }
 	void set(int index, T value)
    {        
        if (index<0 || index>=m_size)
            throw std::out_of_range("SampleContainer: Wrong index " + to_string(idx));
        m_data[index]=value;
    }

    T& operator[](int index)
    {
        if (index<0 || index>=m_size)
            throw std::out_of_range("SampleContainer: Wrong index " + to_string(idx));
        return m_data[index];
    }
    T operator[](int index) const
    {
        if (index<0 || index>=m_size)
            throw std::out_of_range("SampleContainer: Wrong index " + to_string(idx));
        return m_data[index];
    }

 	void push_back(T value)
    {
        if (m_size==m_capacity)
 		    Reallocate();
 	    m_data[m_size++] = value;
    }
    void pop_back()
    {
        if (m_size!=0)
            m_size--;
    }

    SimpleContainer::const_iterator cbegin()
    {

    }
    SimpleContainer::const_iterator cend()
    {
    }
private:
 	void Reallocate()
    {
 	    int newCapacity = m_capacity*2;
 	    T* newData = new T [newCapacity];
 	    std::copy(m_data, m_data+m_capacity, newData);
 	    m_capacity = newCapacity;
 	    delete[] m_data;
 	    m_data = newData;
    }
 	T* m_data;
 	int m_size; // логический размер
 	int m_capacity; // фактический размер
};
