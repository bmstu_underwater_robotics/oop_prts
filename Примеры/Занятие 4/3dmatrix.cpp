#include <iostream>
using namespace std;

const int size = 3;

int main(int argc, char *argv[])
{
       int*** testPointer;
       testPointer = new int**[size];
       for (int i=0; i<size; i++)
       {
            testPointer[i] = new int*[size];
            for (int j=0; j<size; j++)
            {
                 testPointer[i][j] = new int[size];
                 for(int k=0; k<size; k++)
                         testPointer[i][j][k]=100*i+10*j+k;
             }
       }
       for (int i=0; i<size; i++)
       {
             for (int j=0; j<size; j++)
             {
                   for(int k=0; k<size; k++)
                         cout << testPointer[i][j][k] << "\t";
                   cout << endl;
             }
             cout << "\n***********************************\n";
      }
      return 0;
}
