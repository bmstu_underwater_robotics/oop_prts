#include <iostream>
using namespace std;

void substract(double& value)
{
    value -= 1;
}

int main(int argc, char *argv[])
{
    double a = 3.14;
    double& b = a; // Ссылка на переменную a
    // double& c; // Ошибка - ссылка ВСЕГДА 
    // должна быть инициализирована
    cout << "a=" << a << " and b = " << b << endl;
    a = 2.78;
    cout << "a=" << a << " and b = " << b << endl;
    b *=2;
    cout << "a=" << a << " and b = " << b << endl;
    substract(b);
    cout << "a=" << a << " and b = " << b << endl;
    substract(a);
    cout << "a=" << a << " and b = " << b << endl;
    double d = 1;
    b = d; // Здесь мы присваиваем значение! 
        // Сделать так, чтобы b стало ссылкой на d - невозможно
    cout << "a=" << a << " and b = " << b   	<<" and d = " << d << endl;
    return 0;
}
