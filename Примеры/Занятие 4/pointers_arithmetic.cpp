#include <iostream>
using namespace std;

int main(int, char**) 
{
    int array [6];
    long long int shift = (array+5) - array;
    cout << array << endl;
    cout << array+5 << endl;
    cout << "Difference in int "<< shift << endl;
    char* ptrC2 = reinterpret_cast<char*> (array+5);
    char* ptrC1 = reinterpret_cast<char*> (array);
    cout << "Difference in char "<< ptrC2 - ptrC1 << endl;
    short* ptrS1 = reinterpret_cast<short*> (array+5);
    short* ptrS2 = reinterpret_cast<short*> (array);
    cout << "Difference in short "<< ptrS1 - ptrS2 << endl;
    unsigned long long* ptrL1 = reinterpret_cast<unsigned long long*> (array+5);
    unsigned long long* ptrL2 = reinterpret_cast<unsigned long long*> (array);
    cout << "Difference in long long "<< ptrL1 - ptrL2 << endl;

    int* ptr1 = new int;
    auto* ptr2 = new short[10];
    delete ptr1;
    delete[] ptr2;
}
