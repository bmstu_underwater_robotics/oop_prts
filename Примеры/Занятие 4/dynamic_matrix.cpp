#include <iostream>
using namespace std;

int main(int argc, char *argv[])
{ 
 	MakeMatrix(3,3);
 	return 0;
}

void MakeMatrix(int rows, int columns)
{
 	double** pArray = new double* [rows]; // Выделили память
 	for (int i=0; i<rows; i++)
 	{
 		pArray[i] = new double[columns]; // И ещё раз
 		for (int j=0; j<columns; j++)
 		 	pArray[i][j] = 100*i+j;
 	}
 	PrintMatrix(pArray, rows, columns);
 	for (int i=0; i<rows; i++)
 		delete[] pArray[i]; // Удалили массивы строк
 	delete[] pArray; // А теперь вобще всё
}

void PrintMatrix(double** array, int rows, int columns)
{
 	for (int i=0; i<rows; i++)
 	{
 	 	for (int j=0; j<columns; j++)
 		 	cout << array[i][j] << "\t";
 	 	cout << endl;
 	}
}
