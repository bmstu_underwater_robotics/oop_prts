#include <iostream>
#include <cstring> // <string.h>
using namespace std;
int main(int argc, char *argv[])
{
       char* cString = new char[80];
       memcpy (cString, "My sample string", strlen("My sample string"));
       cout << "C-string: " << cString << endl;
       cout << "Length of string: " << strlen(cString) << endl;
       cout << "Size of string: " << sizeof(cString) << endl;
       cString[3] = '\0';
       cout << "\nAfter modification:\n";
       cout << "C-string: " << cString << endl;
       cout << "Length of string: " << strlen(cString) << endl;
       cout << "Size of string: " << sizeof(cString) << endl;
       char secondString[] = " long-long-long text";
       char* resultingString = strcat(cString, secondString);
       cout << resultingString << endl;
       cout << "Length of string: " << strlen(cString) << endl;
       cout << "Size of string: " << sizeof(cString) << endl;
       delete[] cString;
}
