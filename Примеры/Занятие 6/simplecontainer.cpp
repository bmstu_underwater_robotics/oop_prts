#include "simplecontainer.h"

using namespace std;

SimpleContainer::SimpleContainer()
{
      m_size = 0;
      m_capacity = 2;
      m_data = new double[m_capacity];
}

SimpleContainer::SimpleContainer(int size, double value)
{
    m_capacity = size>0? size : 2;
	m_data = new double[m_capacity];
	for (int i=0; i<size; i++)
		m_data[i]=value;
	m_size = size;
}

SimpleContainer::~SimpleContainer()
{
	if (m_data)	//	mdata != 0
		delete[] m_data;
	m_data = nullptr;
}

double SimpleContainer::get(int index) const
{
 	if (index>=0 && index<m_size)
 		return m_data[index];
 	else
 	{
 		std::cerr << "Wrong index " << index << endl;
 		return 0;
 	}
}

void SimpleContainer::set(int index, double value)
{
 	if (index>=0 && index<m_size)
 		m_data[index] = value;
 	else
 		std::cerr << "Wrong index " << index << endl;
}

void SimpleContainer::push_back(double value)
{
 	if (m_size==m_capacity)
 		Reallocate();
 	m_data[m_size++] = value;
}

void SimpleContainer::Reallocate()
{
 	cout << "Now we need to reallocate some memory" << endl;
 	int newCapacity = m_capacity*2;
 	double* newData = new double [newCapacity];
 	std::copy(m_data, m_data+m_capacity, newData);
 	m_capacity = newCapacity;
 	delete[] m_data;
 	m_data = newData;
}



