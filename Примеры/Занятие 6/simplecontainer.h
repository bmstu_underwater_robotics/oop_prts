#ifndef SIMPLECONTAINER_H
#define SIMPLECONTAINER_H

class SimpleContainer
{
public:
	SimpleContainer();
	SimpleContainer(int size, int value=0);
    ~SimpleContainer();

 	int size() const {return m_size;}
 	int capacity() const {return m_capacity;}
 	double get(int index) const;
 	void set(int index, double value);
 	void push_back(double value);

private:
 	void Reallocate();

	double* m_data;
 	int m_size; // логический размер
 	int m_capacity; // фактический размер

};

#endif // SIMPLECONTAINER_H